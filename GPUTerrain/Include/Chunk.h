#pragma once
#include <GL/glew.h>
#include "Math/Vector3.h"
#include "Math/Vector4.h"

struct ChunkVertex
{
	Vector4f PositionAmbo;
	Vector3f Normal;
};

/**
* Represents a cubic space of terrain.
*/
class FChunk
{
public:
	static const uint32_t CHUNK_SIZE = 32;

public:
	FChunk();
	~FChunk();

	/**
	* Enables the vertex buffer for this chunk to 
	* recieve transform feedback data.
	*/
	void StartVertexFeedback();

	/**
	* Disables the vertex buffer for this chunk from
	* recieving transform feedback data.
	*/
	void EndVertexFeedback();

	/**
	* Enables the index buffer for this chunk to
	* recieve transform feedback data.
	*/
	void StartIndexFeedback();

	/**
	* Disables the index buffer for this chunk from
	* recieving transform feedback data.
	*/
	void EndIndexFeedback();

	/**
	* Renders vertex data for this chunk.
	*/
	void Render() const;

	/**
	* Checks if this chunk contains any vertices.
	*/
	bool IsEmpty() const { return mIsEmpty; }

	/**
	* Sets if this chunk contains any vertices.
	*/
	void SetEmpty(const std::true_type);
	void SetEmpty(const std::false_type);

private:
	GLuint   mVertexArray;
	GLuint   mVertexBuffer;
	GLuint   mVertexFeedback;

	GLuint   mIndexBuffer;
	GLuint   mIndexFeedback;

	GLuint mIndexQuery;
	GLuint mIndexCount;

	bool     mIsEmpty;
};

