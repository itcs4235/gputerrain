#pragma once

#include "SFML\Window\Window.hpp"
#include <cstdint>
#include "Math\Vector2.h"

/**
* Base class for OpenGL applications. This base class handles creating an OpenGL context, the
* render loop, and basic event handling. Derived classes will implement a Render and HandleEvent
* function to customize behavior.
*/
class BaseGLApp
{
public:
	/**
	* Constructs an OpenGL application.
	* @param WindowWidth - The width of the window.
	* @param WindowHeight - The height of the window.
	* @param WindowName - The name of the window.
	*/
	BaseGLApp(const uint32_t WindowWidth, const uint32_t WindowHeight, const char* WindowName);

	virtual ~BaseGLApp();

	/**
	* Starts the application.
	*/
	void Start();

protected:
	/**
	* Renders all scene geometry.
 	*/
	virtual void Render() = 0;

	/**
	* Handles window events.
	*/
	virtual void HandleEvent(const sf::Event& Event) = 0;

	/**
	* Retrievs the current window.
	*/
	sf::Window& GetWindow() { return mWindow; }

	void SetMouseReset(bool ShouldReset) { mShouldResetMouse = ShouldReset; }
	bool GetMouseReset() const { return mShouldResetMouse; }

private:
	/**
	* The main rendering loop.
	*/
	void RenderLoop();

	/**
	* Updates clock timers.
	*/
	void UpdateTimers();

	/**
	* Services windows events and passes each event to HandleEvent().
	*/
	void ServiceEvents();

private:
	sf::Window mWindow;
	Vector2ui  mResolution;
	bool       mShouldResetMouse;
};

