#pragma once

#include <cstdint>

namespace GLAttributePosition
{
	enum : uint32_t
	{
		Position = 0,
		Normal = 1,
		Color = 2,
		UV = 3,
		x8_y8_z8_case8 = 4,
		x8_y8_z8_null4_edge4 = 5,
	};
}

namespace GLTextureBindings
{
	enum : uint32_t
	{
		DensityTexture = 1,
		TriTable = 2,
		VertexIDTexture = 3,
		NoiseTexture = 4,
		DepthTexture = 5,
		GrassTexture = 6,
		DirtTexture = 7,
		SSAOSampling = 8,
		SSAOPreBlur = 9
	};
}

namespace GLUniformBindings
{
	enum : uint32_t
	{
		TransformBlock = 0,
		WorldTransformBlock = 1,
		DensityTextureBlock = 2,
		FogParamBlock = 3
	};
}