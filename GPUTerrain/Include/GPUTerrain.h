#pragma once
#include "GLApp.h"
#include "TerrainManager.h"
#include "Rendering\Camera.h"
#include "Rendering\UniformBlockStandard.h"
#include "Rendering\FrameBufferObject.h"
#include "Rendering\IRenderPostProcess.h"

#include <vector>

class FGPUTerrain : public BaseGLApp
{
public:
	FGPUTerrain(const uint32_t WindowWidth, const uint32_t WindowHeight, const char* WindowName);
	~FGPUTerrain();

private:
	void Render() override;

	void HandleEvent(const sf::Event& Event) override;

	void UpdateCamera();

private:
	using PostProcessContainer = std::vector<std::unique_ptr<IRenderPostProcess>>;

private:
	FTerrainManager          mTerrainManager;
	FShaderProgram           mRenderPass;
	EZGL::FUniform           mLightDirection;
	EZGL::FUniform           mLightColor;

	PostProcessContainer     mPostProcesses;
	EZGL::FUniformBlock		 mTransformBlock;
	StandardFrameBuffer      mFrameBuffer;
	FCamera                  mCamera;
	float                    mMoveSpeed;
	float                    mLookSpeed;
};

