#pragma once

#include <GL\glew.h>
#include <cstdarg>
#include <cstdint>
#include "Math\Vector2.h"
#include "Math\Vector3.h"

/**
* Layout:
* - InternalFormat
* - Attachment
*/
struct RenderTextureAttachment
{
	GLenum InternalFormat;
	GLenum Attachment;
	GLenum TextureUnit;
};

enum class FrameBufferTarget : GLenum
{
	Draw = GL_DRAW_FRAMEBUFFER,
	Read = GL_READ_FRAMEBUFFER,
	FrameBuffer = GL_FRAMEBUFFER
};

/**
* A standard OpenGL framebuffer that uses
* readable 2D texture render targets for depth and color.
*/
class StandardFrameBuffer
{	
public:
	StandardFrameBuffer(const Vector2i& Resolution);
	~StandardFrameBuffer();
	
	void StartWrite();
	void EndWrite();

	void BindColorTexture(const GLenum TextureUnit);
	void BindDepthTexture(const GLenum TextureUnit);

	Vector2i GetResolution() const { return mResolution; }

	void CopyToSystemFramebuffer(const Vector2ui SystemResolution, const GLbitfield Buffer, const GLenum Filter);

private:
	enum class Textures : uint32_t
	{
		Color,
		Depth
	};

private:
	GLuint mFBO;
	GLuint mRenderTextures[2];
	Vector2i mResolution;
};