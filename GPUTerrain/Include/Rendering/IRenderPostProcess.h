#pragma once

class IRenderPostProcess
{
public:

	IRenderPostProcess() = default;
	virtual ~IRenderPostProcess() = default;

	virtual void OnPreLightingPass(){}
	virtual void OnPostLightingPass(){}
	virtual void OnPostGUIPass(){}
};

