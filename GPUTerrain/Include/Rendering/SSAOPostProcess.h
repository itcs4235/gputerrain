#pragma once
#include "IRenderPostProcess.h"
#include "Rendering\ShaderProgram.h"
#include "Rendering\UniformBlockStandard.h"
#include "Rendering\Uniform.h"

/**
* Applies a depth based SSAO post-process by sampling
* the depth texture.
*/
class FSSAOPostProcess : public IRenderPostProcess
{
public:
	FSSAOPostProcess();
	~FSSAOPostProcess();

	void OnPostLightingPass() override;

	void SetGlobalAmbient(const Vector3f& Color);

private:
	void SetupShaders();

private:
	FShaderProgram      mSSAOProgram;
	EZGL::FUniform      mSSAOResolution;
	EZGL::FUniform      mSSAOSampleDistance;
	EZGL::FUniform      mSSAODistanceScale;
	EZGL::FUniform      mSSAOIntensity;

	FShaderProgram      mBlurProgram;
	EZGL::FUniform      mBlurResolution;
	EZGL::FUniform      mBlurSize;
	EZGL::FUniform      mGlobalAmbLighting;

	GLuint              mSamplingTexture;
	GLuint              mFrameBuffer;
	GLuint              mSSAOTexture;
};

