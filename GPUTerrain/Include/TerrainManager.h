#pragma once

#include "Rendering\ShaderProgram.h"
#include "Chunk.h"
#include "Math\Vector3.h"
#include "Rendering\UniformBlockStandard.h"
#include "Rendering\Uniform.h"
#include <vector>
#include <unordered_map>
#include <SFML\Graphics\Image.hpp>

/**
* Manages the rendering of all chunks within the scene.
*/
class FTerrainManager
{
public:
	static const int32_t VISIBILITY_DISTANCE;
	static const int32_t CHUNK_COUNT;

public:
	FTerrainManager();
	~FTerrainManager();

	/**
	* Preloads all visible chunks. Should be called at the start
	* of the application.
	*/
	void PreloadTerrain();

	/**
	* Renders all visible terrain.
	*/
	void Render();

	/**
	* Updates the terrain.
	*/
	void Update();

private:
	/**
	* Adds all chunks within the view distance that need to be loaded.
	*/
	void UpdateVisibleChunks();

	/**
	* Generates geometry for chunks that need to be loaded.
	*/
	void UpdateLoadList();

	/**
	* Determines all chunks that are currently visible
	* to the main camera.
	*/
	void ConstructRenderList();

	void LoadShaders();

	// Functions to setup GL data for each shader pass
	void ConstructTextureData();
	void ConstructNonEmptyCellsData();
	void ConstructListVertsToGenData();

	// Function to invoke each shader pass used to generate
	// geometry data for each chunk that needs to be loaded.
	GLuint ListNonEmptyCells();
	void ListVertsToGen();
	void GenerateChunkVertices(FChunk& Chunk);

	void BuildVertexIDTexture();
	void GenerateVertexIndices(FChunk& Chunk);

	/**
	* Function that remaps chunk positions to a normalized range
	* within the view distance. This should be used whenever indexing
	* into the visible chunks list.
	* @param Position - 3D position in chunk space
	*/
	std::size_t GetChunkIndex(const Vector3i& Position) const;

private:
	// Shader passes for each pass in Method 3
	// Shaders for generating a chunk
	FShaderProgram      mBuildDensitiesPass;
	FShaderProgram		mListNonEmptyCellsPass;
	FShaderProgram		mListVertsToGenPass;
	FShaderProgram      mGenVerticesPass;

	// Shaders for generating Index Buffer
	FShaderProgram		mSplatVertIdsPass;
	FShaderProgram		mGenIndicesPass;

	// GLSL Interface block used to hold world position. This will allow this uniform data
	// to be bound across all shaders.
	EZGL::FUniformBlock mWorldPositionUniform; 

	// Buffers for VertexGen pass
	GLuint		mChunkPointsVAO;
	GLuint		mChunkPointsArrayBuffer;

	// Buffers for ListNonEmptyCells pass
	GLuint              mNonEmptyCellsVAO;
	GLuint              mNonEmptyCellsFeedbackBuffer;
	GLuint              mNonEmptyCellsFeedbackObject;

	// Buffers for ListVertsToGen pass
	GLuint              mVertsToGenVAO;
	GLuint              mVertsToGenFeedbackBuffer;
	GLuint              mVertsToGenFeedbackObject;

	GLuint				mDensityTexture; // Texture used to fill in each chunk densities
	EZGL::FUniformBlock mDensityTextureInfoBlock;

	GLuint		mTriTable;       // Texture containing marching cubes triangle lookup table
	GLuint		mVertexIDVol;    // Texture containing the vertex id for each vertex within a chunk
	GLuint		mNoiseTexture;   // Texture containing random noise values for sampling
	GLuint		mGrassTexture;
	GLuint		mDirtTexture;

	Vector3i                       mLastCameraChunk; // Chunk that the camera was in during the last frame
	std::unique_ptr<FChunk[]>      mChunks;          // All visible chunks
	std::unique_ptr<Vector3i[]>    mChunkPositions;  // Chunk position of each loaded chunk in visible list
	std::vector<Vector3i>          mLoadList;        // Chunks that need to be generated
	std::vector<uint32_t>		   mRenderList;		 // All chunks that are within the view volume
};


inline std::size_t FTerrainManager::GetChunkIndex(const Vector3i& Position) const
{
	const int32_t HorizontalBounds = 2 * VISIBILITY_DISTANCE + 1;
	const int32_t VerticalBounds = VISIBILITY_DISTANCE + 1;
	// To account for negative values, 2 mods are needed. The first assures a position number, the second assures a number within the bounds.
	Vector3i NewPosition = Vector3i{ Position.x % HorizontalBounds, Position.y % VerticalBounds, Position.z % HorizontalBounds } +Vector3i{ HorizontalBounds, VerticalBounds, HorizontalBounds };
	NewPosition = Vector3i{ NewPosition.x % HorizontalBounds, NewPosition.y % VerticalBounds, NewPosition.z % HorizontalBounds };
	const Vector3i PositionToIndex{ HorizontalBounds, HorizontalBounds * HorizontalBounds, 1 };
	return Vector3i::Dot(NewPosition, PositionToIndex);
}
