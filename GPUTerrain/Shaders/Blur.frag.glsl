#version 420 core

layout (binding = 9) uniform sampler2D SSAOPreBlur;

precision mediump float;

uniform vec2 Resolution;
const uint BlurFrame = 2;
uniform vec2 BlurSize;
uniform vec3 GlobalAmbLighting;

out vec4 Color;

void main()
{
	vec2 CurrentTexCoord = vec2(gl_FragCoord.xy) / Resolution;

	float Sum;

	for (int y = 0; y < BlurFrame; y++)
	{
		for (int x = 0; x < BlurFrame; x++)
		{
			Sum += texture(SSAOPreBlur, CurrentTexCoord + vec2(x, y) * BlurSize).r;
		}
	}

	Sum /= (BlurFrame * BlurFrame);
	Color = vec4(GlobalAmbLighting * Sum, 1.0);

	// Debug tests
	//Color = vec4(Sum, Sum, Sum, 1);
	//float RawSSAO = texture(SSAOPreBlur, CurrentTexCoord).r;
	//Color = vec4(RawSSAO, RawSSAO, RawSSAO, 1);
}