#version 420 core
precision mediump float;

in uint x8_y8_z8_case8[];

out uint index;

layout(points) in;
layout(points, max_vertices = 15) out;

layout(binding = 2) uniform isampler2D triTable;
layout(binding = 3) uniform usampler3D VertexIDMap;

const int edgeTable[256] = 
{
	0, 1, 1, 2, 1, 2, 2, 3,  1, 2, 2, 3, 2, 3, 3, 2,  1, 2, 2, 3, 2, 3, 3, 4,  2, 3, 3, 4, 3, 4, 4, 3,  
    1, 2, 2, 3, 2, 3, 3, 4,  2, 3, 3, 4, 3, 4, 4, 3,  2, 3, 3, 2, 3, 4, 4, 3,  3, 4, 4, 3, 4, 5, 5, 2,  
    1, 2, 2, 3, 2, 3, 3, 4,  2, 3, 3, 4, 3, 4, 4, 3,  2, 3, 3, 4, 3, 4, 4, 5,  3, 4, 4, 5, 4, 5, 5, 4,  
    2, 3, 3, 4, 3, 4, 2, 3,  3, 4, 4, 5, 4, 5, 3, 2,  3, 4, 4, 3, 4, 5, 3, 2,  4, 5, 5, 4, 5, 2, 4, 1,  
    1, 2, 2, 3, 2, 3, 3, 4,  2, 3, 3, 4, 3, 4, 4, 3,  2, 3, 3, 4, 3, 4, 4, 5,  3, 2, 4, 3, 4, 3, 5, 2,  
    2, 3, 3, 4, 3, 4, 4, 5,  3, 4, 4, 5, 4, 5, 5, 4,  3, 4, 4, 3, 4, 5, 5, 4,  4, 3, 5, 2, 5, 4, 2, 1,  
    2, 3, 3, 4, 3, 4, 4, 5,  3, 4, 4, 5, 2, 3, 3, 2,  3, 4, 4, 5, 4, 5, 5, 2,  4, 3, 5, 4, 3, 2, 4, 1,  
    3, 4, 4, 5, 4, 5, 3, 4,  4, 5, 5, 2, 3, 4, 2, 1,  2, 3, 3, 2, 3, 4, 2, 1,  3, 2, 4, 1, 2, 1, 1, 0 
};

// Specifies which direction to shift to get the neighboring index
const ivec3 shift[12] = {
	ivec3(0, 0, 0), // If edge 0, do not change voxel
	ivec3(0, 1, 0), // If edge 1, move up in y-axis
	ivec3(1, 0, 0), // If edge 2, move right in x axis
	ivec3(0, 0, 0), // If edge 3, do not change voxel
	ivec3(0, 0, 1), // If edge 4, move forward in z-axis
	ivec3(0, 1, 1), // 5
	ivec3(1, 0, 1), // 6
	ivec3(0, 0, 1), // 7
	ivec3(0, 0, 0), // 8
	ivec3(0, 1, 0), // 9
	ivec3(1, 1, 0), // 10
	ivec3(1, 0, 0), // 11
};

// Lookup table for edge offsets
const int EdgeOffsets[12] =
{
	1, 0, 1, 0, 1, 0, 1, 0, 2, 2, 2, 2
};

void main()
{
	uint CaseID = x8_y8_z8_case8[0] & 0xFF;

	ivec3 TexCoord;
	TexCoord.x = int(x8_y8_z8_case8[0] >> 24);
	TexCoord.y = int((x8_y8_z8_case8[0] & 0x00FF0000) >> 16);
	TexCoord.z = int((x8_y8_z8_case8[0] & 0x0000FF00) >> 8);

	uint TriangleCount = edgeTable[CaseID];

	if (max(max(TexCoord.x, TexCoord.y), TexCoord.z) >= 32)
		TriangleCount = 0;

	// Output indices for each triangle
	for(uint t = 0; t < TriangleCount; t++)
	{
		uint TableOffset = t * 3;
		ivec3 VertexEdges = ivec3(texelFetch(triTable, ivec2(TableOffset, CaseID), 0).r, texelFetch(triTable, ivec2(TableOffset + 1, CaseID), 0).r, texelFetch(triTable, ivec2(TableOffset + 2, CaseID), 0).r);

		uvec3 Indices;
		// Get each edge index from vertex id map
		for (int i = 2; i >= 0; i--) // DirectX uses CW winding order, GL uses CCW
		{
			ivec3 sVoxel = TexCoord + shift[VertexEdges[i]];
			sVoxel.x *= 3;
			sVoxel.x += int(EdgeOffsets[VertexEdges[i]]);
			Indices[i] = texelFetch(VertexIDMap, sVoxel, 0).r;
		}

		//if(Indices.x * Indices.y * Indices.z != 0)
		{
			index = Indices[0];
			EmitVertex();

			index = Indices[1];
			EmitVertex();

			index = Indices[2];
			EmitVertex();
		}
	}
}