#version 420 core
precision mediump float;

layout(location = 4) in uint in_x8_y8_z8_case8;

out uint x8_y8_z8_case8;

void main()
{
	x8_y8_z8_case8 = in_x8_y8_z8_case8;
}