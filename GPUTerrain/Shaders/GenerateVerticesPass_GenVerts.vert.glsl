#version 420 core
precision mediump float;

const ivec2 edge_to_verts[12] = {
	ivec2(0, 1), //0
	ivec2(1, 2), //1
	ivec2(2, 3), //2
	ivec2(3, 0), //3
	ivec2(4, 5), //4
	ivec2(5, 6), //5
	ivec2(6, 7), //6
	ivec2(7, 4), //7
	ivec2(0, 4), //8
	ivec2(1, 5), //9
	ivec2(2, 6), //10
	ivec2(3, 7)  //11
};
const ivec3 vert_to_texcoord[8] = {
	ivec3(0, 0, 0), // v0
	ivec3(0, 1, 0), // v1
	ivec3(1, 1, 0), // v2
	ivec3(1, 0, 0), // v3
	ivec3(0, 0, 1), // v4
	ivec3(0, 1, 1), // v5
	ivec3(1, 1, 1), // v6
	ivec3(1, 0, 1)  // v7
};

layout(location = 5) in uint x8_y8_z8_null4_edge4;

out vec4 WorldPosition_Ambient;
out vec3 Normal;

layout (std140, binding = 1) uniform WorldPositionBlock
{
	ivec3 WorldPosition;
};

layout(binding = 1) uniform sampler3D DensityMap;

layout (std140, binding = 2) uniform DensityTextureBlock
{
	int DensityTextureSize;
	int DensityTextureMargin;
};

void main()
{
	// Unpack the int
	ivec3 Position;
	Position.x = int(x8_y8_z8_null4_edge4 >> 24);
	Position.y = int((x8_y8_z8_null4_edge4 & 0x00FF0000) >> 16);
	Position.z = int((x8_y8_z8_null4_edge4 & 0x0000FF00) >> 8);
	uint edge = x8_y8_z8_null4_edge4 & 0x000000FF;

	// Compute the vertex position
	ivec2 verts = edge_to_verts[edge];
	ivec3 EdgeVert1 = vert_to_texcoord[verts.x] + Position;
	ivec3 EdgeVert2 = vert_to_texcoord[verts.y] + Position;
	float VertDensity1 = texelFetch(DensityMap, EdgeVert1 + DensityTextureMargin, 0).r;
	float VertDensity2 = texelFetch(DensityMap, EdgeVert2 + DensityTextureMargin, 0).r;
	
	//vec3 Gradient;
	float PercentToMove = clamp(VertDensity1 / (VertDensity1 - VertDensity2), 0.0, 1.0);
	vec3 Vertex = EdgeVert1 * (1.0 - PercentToMove) + EdgeVert2 * PercentToMove;

	float d = 1.0 / (DensityTextureSize + 2 * DensityTextureMargin);
	vec3 UVW = (Vertex + DensityTextureMargin) * d;

	//// Sample by one unit in each direction
	vec3 Gradient;
	Gradient.x = texture(DensityMap, UVW + vec3(d, 0, 0)).x - texture(DensityMap, UVW + vec3(-d, 0, 0)).x;
	Gradient.y = texture(DensityMap, UVW + vec3(0, d, 0)).x - texture(DensityMap, UVW + vec3(0, -d, 0)).x;
	Gradient.z = texture(DensityMap, UVW + vec3(0, 0, d)).x - texture(DensityMap, UVW + vec3(0, 0, -d)).x;

	// Set Vertex data
	WorldPosition_Ambient = vec4(WorldPosition + Vertex, .5);
	Normal = -normalize(Gradient);
}