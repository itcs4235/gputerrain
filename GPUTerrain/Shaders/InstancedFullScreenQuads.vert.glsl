#version 420 core
precision mediump float;

// Shader for rendering a full screen quad. Use glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, 33);

out flat int InstanceID;

const vec4 QuadVerts[4] = vec4[4](	vec4(-1.0, -1.0, 0.0, 1.0),
									vec4( 1.0, -1.0, 0.0, 1.0),
									vec4(-1.0,  1.0, 0.0, 1.0),
									vec4( 1.0,  1.0, 0.0, 1.0));

void main()
{	
	InstanceID = gl_InstanceID;
	gl_Position = QuadVerts[gl_VertexID];
}