#version 420 core
precision mediump float;

layout(binding = 1) uniform sampler3D DensityMap;

in ivec3 VoxelPosition[];

out uint x8_y8_z8_case8;

layout(points) in;
layout(points, max_vertices = 1) out;

const ivec3 shift[8] = {
	ivec3(0, 0, 0),
	ivec3(0, 1, 0),
	ivec3(1, 1, 0),
	ivec3(1, 0, 0),
	ivec3(0, 0, 1),
	ivec3(0, 1, 1),
	ivec3(1, 1, 1),
	ivec3(1, 0, 1)
};

layout (std140, binding = 2) uniform DensityTextureBlock
{
	int DensityTextureSize;
	int DensityTextureMargin;
};

void main()
{
	ivec3 Position = VoxelPosition[0];
	uint caseID = 0;
	for (int i = 7; i >= 0; --i)
		caseID = caseID | ((texelFetch(DensityMap, DensityTextureMargin + Position + shift[i], 0).r > 0) ? 1 : 0) << i;

	// If there is at least one triangle (therefore, at least one vertex on an edge of this cell)
	if (caseID != 0 && caseID != 255)
	{
		x8_y8_z8_case8 = (uint(Position.x) << 24) | (uint(Position.y) << 16) | (uint(Position.z) << 8) | (caseID);
		EmitVertex();
	}
}