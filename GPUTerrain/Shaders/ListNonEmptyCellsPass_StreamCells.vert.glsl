#version 420 core
precision mediump float;

layout(location = 0) in ivec3 vVoxelPosition;

out ivec3 VoxelPosition;

void main()
{
	VoxelPosition = vVoxelPosition;
}