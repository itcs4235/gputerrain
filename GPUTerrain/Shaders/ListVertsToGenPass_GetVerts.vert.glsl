#version 420 core
precision mediump float;

layout(location = 4) in uint x8_y8_z8_case8;

out uint x8_y8_z8_null5_edgeflags3;

void main()
{
	uint caseID = x8_y8_z8_case8 & 0xFF;

	uint v0 = 1;
	uint v1 = 1 << 1;
	uint v3 = 1 << 3;
	uint v4 = 1 << 4;

	uint edgeFlags = 0;
	uint v0Status = caseID & v0;

	// Check edge 0
	if (v0Status != ((caseID & v1) >> 1))
	{
		edgeFlags |= 1;
	}

	// Check edge 3
	if (v0Status != ((caseID & v3) >> 3))
	{
		edgeFlags |= 2;
	}

	// Check edge 8
	if (v0Status != ((caseID & v4) >> 4))
	{
		edgeFlags |= 4;
	}

	uint x8_y8_z8 = x8_y8_z8_case8 & 0xFFFFFF00;

	x8_y8_z8_null5_edgeflags3 = x8_y8_z8 | edgeFlags;
}