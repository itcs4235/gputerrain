#version 420 core
precision mediump float;

in uint x8_y8_z8_null5_edgeflags3[];

out uint x8_y8_z8_null4_edge4;

layout(points) in;
layout(points, max_vertices = 3) out;

void main()
{
	uint x8_y8_z8_null8 = x8_y8_z8_null5_edgeflags3[0] & 0xFFFFFF00;

	// Edge 0
	if ((x8_y8_z8_null5_edgeflags3[0] & 1) != 0)
	{
		x8_y8_z8_null4_edge4 = x8_y8_z8_null8 | 0;
		EmitVertex();
	}

	// Edge 3
	if ((x8_y8_z8_null5_edgeflags3[0] & (1 << 1)) != 0)
	{
		x8_y8_z8_null4_edge4 = x8_y8_z8_null8 | 3;
		EmitVertex();
	}

	// Edge 8
	if ((x8_y8_z8_null5_edgeflags3[0] & (1 << 2)) != 0)
	{
		x8_y8_z8_null4_edge4 = x8_y8_z8_null8 | 8;
		EmitVertex();
	}
}