#version 420 core
precision mediump float;

in VS_OUT 
{
	vec3 Normal;
	vec3 LightDirection;
	vec3 Viewer;
	vec3 BlendWeight;
	vec3 wsCoord;
} fs_in;


layout (binding = 6) uniform sampler2D GrassTexture;
layout (binding = 7) uniform sampler2D DirtTexture;

uniform vec3 LightColor = vec3(.7,.7,.7);

layout (location = 0) out vec4 Color;

void main()
{
	vec4 col1 = texture(DirtTexture, fs_in.wsCoord.yz);
	vec4 col2 = texture(GrassTexture, fs_in.wsCoord.zx);
	vec4 col3 = texture(DirtTexture, fs_in.wsCoord.xy);

	vec3 BlendColor = col1.xyz * fs_in.BlendWeight.xxx
					  + col2.xyz * fs_in.BlendWeight.yyy
					  + col3.xyz * fs_in.BlendWeight.zzz;

	// Normal Vector Perturbation
	// Needs noise texture

	vec3 N = normalize(fs_in.Normal);

	vec3 L = normalize(fs_in.LightDirection);
	vec3 V = normalize(fs_in.Viewer);
	
	vec3 H = normalize(L + V);

	vec3 Diffuse = max(0.0, dot(N, L)) * BlendColor * LightColor;

	// No spec for dirt
	//vec3 Spec = pow(max( 0.0, dot(H, N)), 1.0) * SpecularProduct;
	//if(dot(N, L) < 0.0)
	//	Spec = vec3(0,0,0);

	Color = vec4(Diffuse, 1.0);
}