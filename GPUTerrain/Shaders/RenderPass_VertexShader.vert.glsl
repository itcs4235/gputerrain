#version 420 core
precision mediump float;

layout(location = 0) in vec4 vPosition_Amb;
layout(location = 1) in vec3 vNormal;

layout(std140, binding = 0) uniform TransformBlock
{
//   Member				Base Align		Aligned Offset		End
	mat4 View;  	 //		16					0			64
	mat4 Projection; //		16					64			128
} Transforms;

out VS_OUT
{
	vec3 Normal;
	vec3 LightDirection;
	vec3 Viewer;
	vec3 BlendWeight;
	vec3 wsCoord;
} vs_out;

uniform vec3 LightDirection = vec3(0,-.937, .377); // Temp directional light

void main()
{
	vec3 BlendWeights = abs(vNormal);
	BlendWeights = (BlendWeights - 0.2) * 7.0;
	BlendWeights = max(BlendWeights, 0.0);
	BlendWeights /= (BlendWeights.x + BlendWeights.y + BlendWeights.z);

	vs_out.BlendWeight = BlendWeights;
	vs_out.wsCoord = vPosition_Amb.xyz * .05; // Space texture UVs out

	vec3 ViewPosition = (Transforms.View * vec4(vPosition_Amb.xyz, 1)).xyz;
	vs_out.Normal = (Transforms.View * vec4(vNormal, 0)).xyz;
	vs_out.LightDirection = (Transforms.View * vec4(-LightDirection, 0)).xyz;
	vs_out.Viewer = -ViewPosition;
	gl_Position = Transforms.Projection * vec4(ViewPosition, 1);
}