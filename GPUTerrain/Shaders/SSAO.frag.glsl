#version 420 core
precision mediump float;

layout (binding = 5) uniform sampler2D DepthTexture;
layout (binding = 8) uniform sampler2D SSAOSampling;
const int SamplingTexSize = 64;

uniform vec2 Resolution = vec2(1500, 1000);
uniform float SampleDistance = 1500; // Max pixel distance to sample, this is scaled with depth

uniform float DistanceScale = 1.0;
uniform float Intensity = 1.0;
uniform float ZBias = 1.25;

const int SampleCount = 4;
const vec2 Samples[SampleCount] = 
{
	vec2(0, -1),
	vec2(0, 1),
	vec2(1, 0),
	vec2(-1, 0)
};

out vec4 Color;


layout(std140, binding = 0) uniform TransformBlock
{
//   Member				Base Align		Aligned Offset		End
	mat4 View;  	 //		16					0			64
	mat4 Projection; //		16					64			128
} Transforms;

vec3 GetViewPosition(sampler2D DepthSampler, ivec2 ScreenCoord, vec2 Resolution)
{
	vec2 NDC = ((ScreenCoord * 2.0) / Resolution) - 1.0;
	float Depth = texelFetch(DepthSampler, ScreenCoord, 0).r * 2.0 - 1.0;
	vec4 View = inverse(Transforms.Projection) * vec4(NDC, Depth, 1.0);
	return View.xyz / View.w;
}


float CalculateAO(vec3 PixelPosition, vec2 PixelCoord, vec2 OffsetCoord)
{
	vec3 SamplePosition = GetViewPosition(DepthTexture, ivec2(PixelCoord + OffsetCoord), Resolution);
	float Distance = distance(PixelPosition, SamplePosition) * DistanceScale;
	return (PixelPosition.z < SamplePosition.z - ZBias) ? (1.0 / (1.0 + Distance)) * Intensity : 0.0;
}

void main()
{	
	// Get info for this pixel
	vec2 ScreenCoord = vec2(gl_FragCoord.xy);
	vec2 CurrentTexCoord = ScreenCoord / Resolution;
	vec2 CurrentSamplingTexCoord = ScreenCoord / float(SamplingTexSize);
	vec2 RandomDirection = normalize(texture(SSAOSampling, CurrentSamplingTexCoord).xy * 2.0 - 1.0);

	vec3 CurrentPosition = GetViewPosition(DepthTexture, ivec2(gl_FragCoord.xy), Resolution);

	// Scale the max sample distance with the distance of this pixel from the camera
	float ScaledRadius = SampleDistance / CurrentPosition.z;

	float Occlusion = 0.0;
	for (int i = 0; i < SampleCount; i++)
	{
		vec2 ReflectedSample = reflect(Samples[i], RandomDirection) * ScaledRadius;
		vec2 RotatedSample = vec2(ReflectedSample.x * 0.707 - ReflectedSample.y * 0.707, ReflectedSample.x * 0.707 + ReflectedSample.y * 0.707);

		Occlusion += CalculateAO(CurrentPosition, ScreenCoord, ReflectedSample * 0.25);
		Occlusion += CalculateAO(CurrentPosition, ScreenCoord, RotatedSample * 0.5);
		Occlusion += CalculateAO(CurrentPosition, ScreenCoord, ReflectedSample * 0.75);
		Occlusion += CalculateAO(CurrentPosition, ScreenCoord, RotatedSample);
	}

	Occlusion /= (SampleCount * 2);
	Occlusion = 1.0 - Occlusion;
	Color = vec4(Occlusion, 0, 0, 0.0);
}