#version 420 core
precision mediump float;

layout(location = 5) in uint x8_y8_z8_null4_edge4;

out flat ivec3 Position;
out flat uint VertexID;

// Lookup table for edge offsets
const int EdgeOffsets[12] =
{
	1, 0, 1, 0, 1, 0, 1, 0, 2, 2, 2, 2
};

void main()
{
	VertexID = gl_VertexID;

	// Edge which can only be 0, 3, or 8
	// If it isn't....something is wrong
	uint Edge = x8_y8_z8_null4_edge4 & 0x0000000F;
	
	Position.x = int((x8_y8_z8_null4_edge4 >> 24) * 3 + EdgeOffsets[Edge]);
	Position.y = int((x8_y8_z8_null4_edge4 & 0x00FF0000) >> 16);
	Position.z = int((x8_y8_z8_null4_edge4 & 0x0000FF00) >> 8);

	gl_Position = vec4(0,0,0,1); // Make sure we always get to the FS
}