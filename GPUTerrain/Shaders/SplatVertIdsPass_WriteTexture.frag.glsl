#version 420 core
precision mediump float;

layout (binding = 3, r32ui) uniform uimage3D VertexIDVol;

in flat ivec3 Position;
in flat uint VertexID;

void main()
{
	imageStore(VertexIDVol, Position, uvec4(VertexID, 0, 0, 0));
}

