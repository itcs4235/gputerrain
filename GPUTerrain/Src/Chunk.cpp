#include "Chunk.h"
#include <cstdint>
#include "Rendering\GLUtils.h"
#include "GLBindings.h"

static const uint32_t MAX_VERTICES = (FChunk::CHUNK_SIZE + 1) * (FChunk::CHUNK_SIZE + 1) * (FChunk::CHUNK_SIZE + 1);
static const uint32_t MAX_INDICES = FChunk::CHUNK_SIZE * FChunk::CHUNK_SIZE * FChunk::CHUNK_SIZE * 15;

FChunk::FChunk()
	: mVertexArray(0)
	, mVertexBuffer(0)
	, mVertexFeedback(0)
	, mIndexBuffer(0)
	, mIndexFeedback(0)
	, mIndexQuery(0)
	, mIndexCount(0)
	, mIsEmpty(true)
{
	// Query for index count
	glGenQueries(1, &mIndexQuery);

	glGenBuffers(1, &mVertexBuffer);
	glGenBuffers(1, &mIndexBuffer);

	// Generate feedback vertex object and bind feedback buffer
	glGenTransformFeedbacks(1, &mVertexFeedback);
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, mVertexFeedback);
		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, mVertexBuffer);
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);

	// Generate feedback index object and bind feedback buffer
	glGenTransformFeedbacks(1, &mIndexFeedback);
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, mIndexFeedback);
		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, mIndexBuffer);
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);

	// Generate vertex array object and enable feedback buffer attributes
	glGenVertexArrays(1, &mVertexArray);
	glBindVertexArray(mVertexArray);
		glBindBuffer(GL_ARRAY_BUFFER, mVertexBuffer);
			glVertexAttribPointer(GLAttributePosition::Position, 4, GL_FLOAT, GL_FALSE, sizeof(ChunkVertex), BUFFER_OFFSET(0));
			glVertexAttribPointer(GLAttributePosition::Normal, 3, GL_FLOAT, GL_FALSE, sizeof(ChunkVertex), BUFFER_OFFSET(sizeof(Vector4f)));
			glEnableVertexAttribArray(GLAttributePosition::Position);
			glEnableVertexAttribArray(GLAttributePosition::Normal);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffer);
	glBindVertexArray(0);
}


FChunk::~FChunk()
{
	glDeleteVertexArrays(1, &mVertexArray);
	glDeleteBuffers(1, &mVertexBuffer);
	glDeleteBuffers(1, &mIndexBuffer);
	glDeleteTransformFeedbacks(1, &mIndexFeedback);
	glDeleteTransformFeedbacks(1, &mVertexFeedback);
}

void FChunk::SetEmpty(const std::true_type)
{
	// Deallocate buffers
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, 0, nullptr, GL_STATIC_COPY);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, 0, nullptr, GL_STATIC_COPY);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	mIsEmpty = true;
}

void FChunk::SetEmpty(const std::false_type)
{
	// Allocate buffers
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(ChunkVertex) * MAX_VERTICES, nullptr, GL_STATIC_COPY);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32_t) * MAX_INDICES, nullptr, GL_STATIC_COPY);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	mIsEmpty = false;
}

void FChunk::StartVertexFeedback()
{
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, mVertexFeedback);
	glBeginTransformFeedback(GL_POINTS);
}

void FChunk::EndVertexFeedback()
{
	glEndTransformFeedback();

	// Debugging: Check vertex data
#ifndef NDEBUG
	glFlush();
	ChunkVertex* Verts = new ChunkVertex[MAX_VERTICES];
	glGetBufferSubData(GL_TRANSFORM_FEEDBACK_BUFFER, 0, sizeof(ChunkVertex) * MAX_VERTICES, Verts);
	delete[] Verts;
#endif

	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);
}

void FChunk::StartIndexFeedback()
{
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, mIndexFeedback);
	glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, mIndexQuery);
	glBeginTransformFeedback(GL_POINTS);
}

void FChunk::EndIndexFeedback()
{
	glEndTransformFeedback();

	// Get indices count
	glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);
	glGetQueryObjectuiv(mIndexQuery, GL_QUERY_RESULT, &mIndexCount);

	// Debugging: Check index data
#ifndef NDEBUG
	glFlush();
	uint32_t* Indices = new uint32_t[MAX_INDICES];
	glGetBufferSubData(GL_TRANSFORM_FEEDBACK_BUFFER, 0, sizeof(uint32_t) * MAX_INDICES, Indices);
	delete[] Indices;
#endif

	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);
}

void FChunk::Render() const
{
	// Bind vertex array object and draw feedback data
	glBindVertexArray(mVertexArray);
	glDrawElements(GL_TRIANGLES, mIndexCount, GL_UNSIGNED_INT, BUFFER_OFFSET(0));
}