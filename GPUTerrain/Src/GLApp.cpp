#include "GLApp.h"
#include <SFML\Window\Context.hpp>
#include <SFML\Window\Event.hpp>
#include <SFML\Window\VideoMode.hpp>
#include "Clock.h"
#include "STime.h"
#include "Input\ButtonEvent.h"
#include "Input\MouseAxis.h"
#include "SystemResources\SystemFile.h"
#include "Rendering\Screen.h"

#include <gl\glew.h>
#include <cstdint>

static const float FPS = 30.0f;

BaseGLApp::BaseGLApp(const uint32_t WindowWidth, const uint32_t WindowHeight, const char* WindowName)
	: mWindow(sf::VideoMode{ WindowWidth, WindowHeight }, WindowName, sf::Style::Default, sf::ContextSettings(24, 8, 0, 4, 2))
	, mResolution(WindowWidth, WindowHeight)
	, mShouldResetMouse(false)
{
	if (glewInit()) {
		std::cerr << "Unable to initialize GLEW ... exiting" << std::endl;
		exit(EXIT_FAILURE);
	}

	// Allocate the file system
	IFileSystem* FileSystem = new FFileSystem;

	SScreen::SetResolution(Vector2ui{ WindowWidth, WindowHeight });
	SMouseAxis::SetDefaultMousePosition(Vector2i{ (int32_t)WindowWidth, (int32_t)WindowHeight } / 2);
}

BaseGLApp::~BaseGLApp()
{
	delete IFileSystem::GetInstancePtr();
}

void BaseGLApp::Start()
{
	RenderLoop();
}

void BaseGLApp::RenderLoop()
{
	STime::SetDeltaTime(1.0f / FPS); // Default delta time with 30fps

	// Game Loop
	while (mWindow.isOpen())
	{
		Render();
		mWindow.display();

		UpdateTimers();
		ServiceEvents();
	}
}

void BaseGLApp::UpdateTimers()
{
	static FClock GameTimer;
	static uint64_t FrameStart = FClock::ReadSystemTimer(), FrameEnd;

	// Manage frame timers
	FrameEnd = FClock::ReadSystemTimer();
	uint64_t FrameTime = FrameEnd - FrameStart;

	// Update the game clock with this frame's time and
	// compute the frame delta time by taking the game timer's time
	// before and after this update (GameTimer may be time scaled)
	const uint64_t PreUpdateTimer = GameTimer.GetCycles();
	GameTimer.Update(FClock::CyclesToSeconds(FrameTime));
	const uint64_t PostUpdateTimer = GameTimer.GetCycles();

	float DeltaTime = FClock::CyclesToSeconds(PostUpdateTimer - PreUpdateTimer);

	// If large delta time, we were probably in a breakpoint
	if (DeltaTime > 1.5f)
	{
		DeltaTime = 1.0f / 30.0f;
	}

	// Set delta time for this frame
	STime::SetDeltaTime(DeltaTime);
	FrameStart = FrameEnd;
}

void BaseGLApp::ServiceEvents()
{
	// Windows events
	sf::Event Event;

	// Reset button and axes from previous frame
	SButtonEvent::ResetButtonEvents();
	SMouseAxis::ResetAxes();

	if (mShouldResetMouse)
	{
		SMouseAxis::UpdateDelta(mWindow);
		sf::Mouse::setPosition(sf::Vector2i{ (int32_t)mResolution.x / 2, (int32_t)mResolution.y / 2 }, mWindow);
	}

	// Service window events
	while (mWindow.pollEvent(Event))
	{
		if (Event.type == Event.Closed)
			mWindow.close();
		else if (SButtonEvent::IsButtonEvent(Event))
		{
			SButtonEvent::AddButtonEvent(Event);
		}
		else if (SMouseAxis::IsMouseAxisEvent(Event))
		{
			SMouseAxis::UpdateEvent(Event);
		}
		else if (Event.type == sf::Event::Resized)
		{
			// adjust the viewport when the window is resized
			glViewport(0, 0, Event.size.width, Event.size.height);
		}

		HandleEvent(Event);
	}
}