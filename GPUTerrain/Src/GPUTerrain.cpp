#include "..\Include\GPUTerrain.h"
#include "GL\glew.h"
#include "Math\PerspectiveMatrix.h"
#include "Rendering\GLUtils.h"
#include "Input\ButtonEvent.h"
#include "STime.h"
#include "Input\MouseAxis.h"
#include "GLBindings.h"
#include "Rendering\FrameBufferObject.h"
#include "Rendering\Screen.h"
#include "Rendering\FFogPostProcess.h"
#include "Rendering\SSAOPostProcess.h"

namespace
{
	struct TransformBlock
	{
		enum
		{
			View = 0,
			Projection = 64,
			Size = 128
		};
	};
}

FGPUTerrain::FGPUTerrain(const uint32_t WindowWidth, const uint32_t WindowHeight, const char* WindowName)
	: BaseGLApp(WindowWidth, WindowHeight, WindowName)
	, mTerrainManager()
	, mRenderPass()
	, mLightDirection()
	, mLightColor()
	, mPostProcesses()
	, mTransformBlock(GLUniformBindings::TransformBlock, TransformBlock::Size)
	, mFrameBuffer(Vector2ui{ WindowWidth, WindowHeight })
	, mCamera()
	, mMoveSpeed(10.0f)
	, mLookSpeed(5.0f)
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	GetWindow().setMouseCursorVisible(false);
	mCamera.SetProjection(FPerspectiveMatrix{(float)WindowWidth, (float)WindowHeight, 35.0f, 0.1f, 420.0f });
	mCamera.Transform.SetPosition(Vector3f{ 100, 12, 220 });
	mTransformBlock.SetData(TransformBlock::Projection, mCamera.GetProjection());


	FShader RenderVertShader{ L"Shaders/RenderPass_VertexShader.vert.glsl", GL_VERTEX_SHADER };
	FShader RenderFragShader{ L"Shaders/RenderPass_FragmentShader.frag.glsl", GL_FRAGMENT_SHADER };
	mRenderPass.AttachShader(RenderVertShader);
	mRenderPass.AttachShader(RenderFragShader);
	mRenderPass.LinkProgram();
	mRenderPass.Use();

	// Setup directional light
	const Vector3f LightDirection = Vector3f{ 0.0f, -0.937f, 0.377f }.Normalize();
	const Vector3f LightColor{ 0.7f, 0.7f, 0.7f };

	mLightDirection.Bind(mRenderPass.GetID(), "LightDirection");
	mLightDirection.SetVector(1, &LightDirection);

	mLightColor.Bind(mRenderPass.GetID(), "LightColor");
	mLightColor.SetVector(1, &LightColor);

	/////////////////////////////////////////////////////////
	///////// Enable Post Processes /////////////////////////
	/////////////////////////////////////////////////////////

	FFogPostProcess* FogPostProcess = new FFogPostProcess;
	FogPostProcess->SetBounds(0.0f, 1.0f);
	FogPostProcess->SetColor(Vector3f{ .6f, .6f, .6f });
	FogPostProcess->SetDensity(.00005f);
	mPostProcesses.push_back(std::unique_ptr<IRenderPostProcess>{FogPostProcess});

	FSSAOPostProcess* SSAO = new FSSAOPostProcess;
	mPostProcesses.push_back(std::unique_ptr<IRenderPostProcess>{SSAO});
	mTerrainManager.PreloadTerrain();
}


FGPUTerrain::~FGPUTerrain()
{
	
}


void FGPUTerrain::Render()
{
	// Toggle mouse lock with L key for camera control
	if (SButtonEvent::GetKeyDown(sf::Keyboard::L))
		SetMouseReset(!GetMouseReset());

	UpdateCamera();
	mTransformBlock.SetData(TransformBlock::View, mCamera.Transform.WorldToLocalMatrix());

	//Vector3i Position = mCamera.Transform.GetPosition() / FChunk::CHUNK_SIZE;
	//std::cout << "Chunk Position: " << Position.x << " " << Position.y << " " << Position.z << std::endl;
	//std::cout << "FPS: " << 1.0f / STime::GetDeltaTime() << std::endl;

	// Debug: Frustum culling
	//FCamera FakeCam;
	//FCamera::Main = &FakeCam;
	//FakeCam.Transform.SetPosition(Vector3f{ 0, 16, 0 });
	//FakeCam.SetProjection(FPerspectiveMatrix{ (float)1500, (float)1000, 35.0f, 0.1f, 400.0f });

	mTerrainManager.Update();

	const Vector2ui Resolution = SScreen::GetResolution();
	glViewport(0, 0, Resolution.x, Resolution.y);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	mRenderPass.Use();

	mFrameBuffer.StartWrite();
	mTerrainManager.Render();
	mFrameBuffer.EndWrite();
	mFrameBuffer.CopyToSystemFramebuffer(SScreen::GetResolution(), GL_COLOR_BUFFER_BIT, GL_NEAREST);

	mFrameBuffer.BindDepthTexture(GL_TEXTURE0 + GLTextureBindings::DepthTexture);
	for (auto& PostProcess : mPostProcesses)
	{
		PostProcess->OnPostLightingPass();
	}
}

void FGPUTerrain::UpdateCamera()
{
	float ZMovement = 0, XMovement = 0, YMovement = 0;
	float MoveSpeed = mMoveSpeed * STime::GetDeltaTime() * (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift) ? 2.0f : 1.0f);

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		ZMovement = MoveSpeed;
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		ZMovement = -MoveSpeed;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		XMovement = MoveSpeed;
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		XMovement = -MoveSpeed;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::E))
		YMovement = MoveSpeed;
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
		YMovement = -MoveSpeed;

	const float LookSpeed = mLookSpeed * STime::GetDeltaTime();
	FQuaternion CameraRotation = mCamera.Transform.GetRotation();

	Vector3f CameraForward = CameraRotation * Vector3f::Forward;
	CameraForward.y = 0.0f;
	CameraForward.Normalize();

	Vector3f CameraRight = Vector3f::Cross(CameraForward, Vector3f::Up);

	CameraRotation = FQuaternion{ CameraRight, (float)SMouseAxis::GetDelta().y * LookSpeed } *CameraRotation;
	CameraRotation = FQuaternion{ Vector3f::Up, -(float)SMouseAxis::GetDelta().x * LookSpeed } *CameraRotation;

	Vector3f NewUp = CameraRotation * -Vector3f::Up;

	// Only rotate if our new up is not pointing towards the ground
	if (Vector3f::Dot(NewUp, Vector3f::Up) > 0)
		mCamera.Transform.SetRotation(CameraRotation);

	const Vector3f Translation = Vector3f{ -XMovement, -YMovement, ZMovement };
	mCamera.Transform.Translate(Translation);
}

void FGPUTerrain::HandleEvent(const sf::Event& Event)
{

}