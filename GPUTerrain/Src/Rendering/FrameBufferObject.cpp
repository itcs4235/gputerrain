#include "..\..\Include\Rendering\FrameBufferObject.h"

StandardFrameBuffer::StandardFrameBuffer(const Vector2i& Resolution)
	: mFBO(0)
	, mResolution(Resolution)
{
	glGenFramebuffers(1, &mFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, mFBO);

	glGenTextures(2, mRenderTextures);

	glBindTexture(GL_TEXTURE_2D, mRenderTextures[(uint32_t)Textures::Color]);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA32F, Resolution.x, Resolution.y);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, mRenderTextures[(uint32_t)Textures::Color], 0);

	glBindTexture(GL_TEXTURE_2D, mRenderTextures[(uint32_t)Textures::Depth]);
		glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT32F, Resolution.x, Resolution.y);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, mRenderTextures[(uint32_t)Textures::Depth], 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

StandardFrameBuffer::~StandardFrameBuffer()
{
	glDeleteFramebuffers(1, &mFBO);
	glDeleteTextures(2, mRenderTextures);
}

void StandardFrameBuffer::StartWrite()
{
	glBindFramebuffer(GL_FRAMEBUFFER, mFBO);
	glViewport(0, 0, mResolution.x, mResolution.y);

	static const GLuint uInt_Zeros[] = { 0, 0, 0, 0 };
	static const GLfloat Float_Ones[] = { 1.0f, 1.0f, 1.0f, 1.0f };

	glDrawBuffer(GL_COLOR_ATTACHMENT0);
	glClearBufferuiv(GL_COLOR, 0, uInt_Zeros);
	glClearBufferfv(GL_DEPTH, 0, Float_Ones);
}

void StandardFrameBuffer::EndWrite()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glDrawBuffer(GL_BACK);
}

void StandardFrameBuffer::CopyToSystemFramebuffer(const Vector2ui SystemResolution, const GLbitfield Buffer, const GLenum Filter)
{
	glBindFramebuffer(GL_READ_FRAMEBUFFER, mFBO);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

	glViewport(0, 0, SystemResolution.x, SystemResolution.y);
	glClear(Buffer);

	glBlitFramebuffer(0, 0, mResolution.x, mResolution.y, 0, 0, mResolution.x, mResolution.y, Buffer, Filter);
}


void StandardFrameBuffer::BindColorTexture(const GLenum TextureUnit)
{
	glActiveTexture(TextureUnit);
	glBindTexture(GL_TEXTURE_2D, mRenderTextures[(uint32_t)Textures::Color]);
}

void StandardFrameBuffer::BindDepthTexture(const GLenum TextureUnit)
{
	glActiveTexture(TextureUnit);
	glBindTexture(GL_TEXTURE_2D, mRenderTextures[(uint32_t)Textures::Depth]);
}
