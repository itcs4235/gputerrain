#include "Rendering\SSAOPostProcess.h"
#include "GLBindings.h"
#include "SFML\Graphics\Texture.hpp"
#include "Rendering\Screen.h"


FSSAOPostProcess::FSSAOPostProcess()
	: mSSAOProgram()
	, mSSAOResolution()
	, mSSAOSampleDistance()
	, mSSAODistanceScale()
	, mSSAOIntensity()
	, mBlurProgram()
	, mBlurResolution()
	, mBlurSize()
	, mGlobalAmbLighting()
	, mSamplingTexture(0)
	, mFrameBuffer(0)
	, mSSAOTexture(0)
{
	SetupShaders();

	// Load random SSAO sampling directions
	sf::Image SamplingImage;
	if (!SamplingImage.loadFromFile("SSAOSamplingDirections.png")) std::cout << "Could not load SSAO sampling texture.\n";
	glGenTextures(1, &mSamplingTexture);
	glActiveTexture(GL_TEXTURE0 + GLTextureBindings::SSAOSampling);
	glBindTexture(GL_TEXTURE_2D, mSamplingTexture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, SamplingImage.getSize().x, SamplingImage.getSize().y, 0, GL_RGBA, GL_UNSIGNED_BYTE, SamplingImage.getPixelsPtr());
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glActiveTexture(GL_TEXTURE0);

	// Generate framebuffer and texture used for SSAO pass
	glGenFramebuffers(1, &mFrameBuffer);
	glGenTextures(1, &mSSAOTexture);
	
	// Allocate and bind texture to framebuffer target
	glBindFramebuffer(GL_FRAMEBUFFER, mFrameBuffer);
	glBindTexture(GL_TEXTURE_2D, mSSAOTexture);
	    glTexStorage2D(GL_TEXTURE_2D, 1, GL_R32F, SScreen::GetResolution().x, SScreen::GetResolution().y);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, mSSAOTexture, 0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void FSSAOPostProcess::SetupShaders()
{
	FShader VertShader{ L"Shaders/FullScreenQuad.vert.glsl", GL_VERTEX_SHADER };
	FShader SSAOShader{ L"Shaders/SSAO.frag.glsl", GL_FRAGMENT_SHADER };
	mSSAOProgram.AttachShader(VertShader);
	mSSAOProgram.AttachShader(SSAOShader);
	mSSAOProgram.LinkProgram();
	mSSAOProgram.Use();

	mSSAOResolution.Bind(mSSAOProgram.GetID(), "Resolution");
	const Vector2f Resolution = SScreen::GetResolution();
	mSSAOResolution.SetVector(1, &Resolution);

	mSSAOSampleDistance.Bind(mSSAOProgram.GetID(), "SampleDistance");
	mSSAOSampleDistance.SetUniform(Resolution.x);

	mSSAODistanceScale.Bind(mSSAOProgram.GetID(), "DistanceScale");
	mSSAODistanceScale.SetUniform(1.0f);

	mSSAOIntensity.Bind(mSSAOProgram.GetID(), "Intensity");
	mSSAOIntensity.SetUniform(1.0f);

	FShader BlurShader{ L"Shaders/Blur.frag.glsl", GL_FRAGMENT_SHADER };
	mBlurProgram.AttachShader(VertShader);
	mBlurProgram.AttachShader(BlurShader);
	mBlurProgram.LinkProgram();
	mBlurProgram.Use();

	mBlurResolution.Bind(mBlurProgram.GetID(), "Resolution");
	mBlurResolution.SetVector(1, &Resolution);

	mBlurSize.Bind(mBlurProgram.GetID(), "BlurSize");
	const Vector2f BlurSize{ 1.0f / 500.0f, 1.0f / 400.0f };
	mBlurSize.SetVector(1, &BlurSize);

	mGlobalAmbLighting.Bind(mBlurProgram.GetID(), "GlobalAmbLighting");
	const Vector3f AmbientLight{ 0.1f, 0.1f, 0.1f };

	// Uncomment this
	//const Vector3f AmbientLight{ .7f, .7f, .7f };
	mGlobalAmbLighting.SetVector(1, &AmbientLight);
}

FSSAOPostProcess::~FSSAOPostProcess()
{
	glDeleteFramebuffers(1, &mFrameBuffer);
	glDeleteTextures(1, &mSSAOTexture);
	glDeleteTextures(1, &mSamplingTexture);
}


void FSSAOPostProcess::OnPostLightingPass()
{
	// bind framebuffer to render SSAO output to
	glBindFramebuffer(GL_FRAMEBUFFER, mFrameBuffer);
	glViewport(0, 0, SScreen::GetResolution().x, SScreen::GetResolution().y);

	static const GLuint uInt_Zeros[] = { 0, 0, 0, 0 };
	glClearBufferuiv(GL_COLOR, 0, uInt_Zeros);
	glDrawBuffer(GL_COLOR_ATTACHMENT0);

	mSSAOProgram.Use();
	glDisable(GL_DEPTH_TEST);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glDrawBuffer(GL_BACK);

	// Blur the AO results and add ambient lighting
	mBlurProgram.Use();
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);
	glBlendEquation(GL_FUNC_ADD);

	// Uncomment this
	//glDisable(GL_BLEND);

	glActiveTexture(GL_TEXTURE0 + GLTextureBindings::SSAOPreBlur);
	glBindTexture(GL_TEXTURE_2D, mSSAOTexture);

	glActiveTexture(GL_TEXTURE0);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	glDisable(GL_BLEND);
}

void FSSAOPostProcess::SetGlobalAmbient(const Vector3f& Color)
{
	mBlurProgram.Use();
	mGlobalAmbLighting.SetVector(1, &Color);
}