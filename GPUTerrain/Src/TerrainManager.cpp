#include "..\Include\TerrainManager.h"
#include "Rendering\GLUtils.h"
#include "GLBindings.h"
#include "Rendering\Camera.h"
#include "..\Include\MarchingCubesLookup.h"
#include <cstdlib>
#include <time.h>
#include "Input\ButtonEvent.h"
#include "Rendering\Screen.h"

static const Vector3i INVALID_CHUNK_POSITION = Vector3i{ 0x7FFFFFFF, 0x7FFFFFFF, 0x7FFFFFFF }; // Largest signed int
static const uint32_t DENSITY_TEXTURE_MARGINS = 2;
static const uint32_t DENSITY_TEXTURE_SIZE = 33;
static const uint32_t DENSITY_TEXTURE_BOUNDS = DENSITY_TEXTURE_SIZE + 2 * DENSITY_TEXTURE_MARGINS;
static const uint32_t MAX_LOADS_PER_FRAME = 2;

namespace
{
	struct TransformBlock
	{
		enum
		{
			WorldPosition = 0,
			Size = 16
		};
	};

	struct DensityTextureBlock
	{
		enum
		{
			Size = 0,
			Margin = 4
		};
	};
}

const int32_t FTerrainManager::VISIBILITY_DISTANCE = 8;
const int32_t FTerrainManager::CHUNK_COUNT = (2 * VISIBILITY_DISTANCE + 1) * (VISIBILITY_DISTANCE + 1) * (2 * VISIBILITY_DISTANCE + 1);

FTerrainManager::FTerrainManager()
	: mBuildDensitiesPass()
	, mListNonEmptyCellsPass()
	, mListVertsToGenPass()
	, mGenVerticesPass()
	, mSplatVertIdsPass()
	, mGenIndicesPass()
	, mWorldPositionUniform(GLUniformBindings::WorldTransformBlock, TransformBlock::Size)
	, mChunkPointsVAO(0)
	, mChunkPointsArrayBuffer(0)
	, mNonEmptyCellsVAO(0)
	, mNonEmptyCellsFeedbackBuffer(0)
	, mNonEmptyCellsFeedbackObject(0)
	, mVertsToGenFeedbackBuffer(0)
	, mVertsToGenFeedbackObject(0)
	, mDensityTexture(0)
	, mDensityTextureInfoBlock(GLUniformBindings::DensityTextureBlock, 8)
	, mTriTable(0)
	, mVertexIDVol(0)
	, mNoiseTexture(0)
	, mGrassTexture(0)
	, mDirtTexture(0)
	, mLastCameraChunk(INVALID_CHUNK_POSITION)
	, mChunks(new FChunk[CHUNK_COUNT])
	, mChunkPositions(new Vector3i[CHUNK_COUNT])
	, mLoadList()
	, mRenderList()
{
	LoadShaders();

	for (uint32_t i = 0; i < CHUNK_COUNT; i++)
	{
		mChunkPositions[i] = INVALID_CHUNK_POSITION;
	}

	ConstructTextureData();
	ConstructNonEmptyCellsData();
	ConstructListVertsToGenData();

	mDensityTextureInfoBlock.SetData(DensityTextureBlock::Size, (uint8_t*)&DENSITY_TEXTURE_SIZE, sizeof(int32_t));
	mDensityTextureInfoBlock.SetData(DensityTextureBlock::Margin, (uint8_t*)&DENSITY_TEXTURE_MARGINS, sizeof(int32_t));

	// Verts sent on the GenVertices pass
	Vector3i GeoPoints[FChunk::CHUNK_SIZE + 1][FChunk::CHUNK_SIZE + 1][FChunk::CHUNK_SIZE + 1];
	for (int32_t z = 0; z < FChunk::CHUNK_SIZE + 1; z++)
	{
	for (int32_t y = 0; y < FChunk::CHUNK_SIZE + 1; y++)
	{
		for (int32_t x = 0; x < FChunk::CHUNK_SIZE + 1; x++)
		{
				GeoPoints[z][y][x] = Vector3i{ x, y, z };
			}
		}
	}

	// Setup vertex buffer used to invoke the geometry shader for each voxel in a chunk.
	glGenVertexArrays(1, &mChunkPointsVAO);
	glBindVertexArray(mChunkPointsVAO);
		glGenBuffers(1, &mChunkPointsArrayBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, mChunkPointsArrayBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GeoPoints), GeoPoints, GL_STATIC_DRAW); // Using immutable storage since this data will never change.
		glVertexAttribIPointer(GLAttributePosition::Position, 3, GL_INT, 0, BUFFER_OFFSET(0));
		glEnableVertexAttribArray(GLAttributePosition::Position);
	glBindVertexArray(0);
}

FTerrainManager::~FTerrainManager()
{
	glDeleteVertexArrays(1, &mChunkPointsVAO);
	glDeleteVertexArrays(1, &mNonEmptyCellsVAO);
	glDeleteVertexArrays(1, &mVertsToGenVAO);
	glDeleteBuffers(1, &mChunkPointsArrayBuffer);
	glDeleteBuffers(1, &mNonEmptyCellsFeedbackBuffer);
	glDeleteBuffers(1, &mVertsToGenFeedbackBuffer);
	glDeleteTextures(1, &mDensityTexture);
	glDeleteTextures(1, &mTriTable);
	glDeleteTextures(1, &mVertexIDVol);
	glDeleteTextures(1, &mNoiseTexture);
	glDeleteTextures(1, &mGrassTexture);
	glDeleteTextures(1, &mDirtTexture);
	glDeleteTransformFeedbacks(1, &mNonEmptyCellsFeedbackObject);
	glDeleteTransformFeedbacks(1, &mVertsToGenFeedbackObject);
}

void FTerrainManager::PreloadTerrain()
{
	ASSERT(FCamera::Main && "No active camera.");
	const Vector3i CameraChunk = FCamera::Main->Transform.GetPosition() / (int32_t)FChunk::CHUNK_SIZE;

	mLastCameraChunk = CameraChunk;
	UpdateVisibleChunks();

	// Load all visible chunks
	while (!mLoadList.empty())
		UpdateLoadList();
}

void FTerrainManager::ConstructTextureData()
{
	// Generate and bind density texture
	glGenTextures(1, &mDensityTexture);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_3D, mDensityTexture);
	glTexStorage3D(GL_TEXTURE_3D, 1, GL_R32F, DENSITY_TEXTURE_BOUNDS, DENSITY_TEXTURE_BOUNDS, DENSITY_TEXTURE_BOUNDS);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glBindImageTexture(GLTextureBindings::DensityTexture, mDensityTexture, 0, GL_TRUE, 0, GL_READ_WRITE, GL_R32F);

	// Generate and bind triTable
	glGenTextures(1, &mTriTable);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, mTriTable);
		glTexStorage2D(GL_TEXTURE_2D, 1, GL_R32I, 16, 256);
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 16, 256, GL_RED_INTEGER, GL_INT, triTable);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		// Debug: Check TriTable
#ifndef NDEBUG
	glFlush();
	int Ptr[256][16];
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RED_INTEGER, GL_INT, Ptr);
#endif

	// Generate and bind vertex ID texture
	glGenTextures(1, &mVertexIDVol);
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_3D, mVertexIDVol);
		glTexStorage3D(GL_TEXTURE_3D, 1, GL_R32UI, 3 * 33, 33, 33);
		glBindImageTexture(GLTextureBindings::VertexIDTexture, mVertexIDVol, 0, GL_TRUE, 0, GL_READ_WRITE, GL_R32UI);

	//srand(time(nullptr));
	float noiseVals[16][16][16];
	for (int i = 0; i < 16; ++i){
		for (int j = 0; j < 16; ++j){
			for (int k = 0; k < 16; ++k){
				noiseVals[i][j][k] = -1.0f+2.0f*((float)rand())/RAND_MAX; // Random value in [-1, 1]
			}
		}
	}

	glGenTextures(1, &mNoiseTexture);
	glActiveTexture(GL_TEXTURE0 + GLTextureBindings::NoiseTexture);
	glBindTexture(GL_TEXTURE_3D, mNoiseTexture);
		glTexStorage3D(GL_TEXTURE_3D, 1, GL_R32F, 16, 16, 16);
		glTexSubImage3D(GL_TEXTURE_3D, 0, 0, 0, 0, 16, 16, 16, GL_RED, GL_FLOAT, noiseVals);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_REPEAT);

	sf::Image GrassPNG;
	if (!GrassPNG.loadFromFile("GrassTexture.png")) std::cout << "Could not load grass texture.\n";
	glGenTextures(1, &mGrassTexture);
	glActiveTexture(GL_TEXTURE0 + GLTextureBindings::GrassTexture);
	glBindTexture(GL_TEXTURE_2D, mGrassTexture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, GrassPNG.getSize().x, GrassPNG.getSize().y, 0, GL_RGBA, GL_UNSIGNED_BYTE, GrassPNG.getPixelsPtr());
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		// Debug: Check Grass Texture
#ifndef NDEBUG
		glFlush();
		struct RGBA
		{
			uint8_t R;
			uint8_t G;
			uint8_t B;
			uint8_t A;
		};
		RGBA* GrassPtr = new RGBA[1024 * 1024 * 3];
		glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, GrassPtr);
		auto Error = gluErrorString(glGetError());
		delete[] GrassPtr;
#endif

	sf::Image DirtPNG;
	if (!DirtPNG.loadFromFile("DirtTexture.png")) std::cout << "Could not load dirt texture.\n";
	glGenTextures(1, &mDirtTexture);
	glActiveTexture(GL_TEXTURE0 + GLTextureBindings::DirtTexture);
	glBindTexture(GL_TEXTURE_2D, mDirtTexture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, DirtPNG.getSize().x, DirtPNG.getSize().y, 0, GL_RGBA, GL_UNSIGNED_BYTE, DirtPNG.getPixelsPtr());
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		// Debug: Check Dirt Texture
#ifndef NDEBUG
		glFlush();
		struct RGB
		{
			uint8_t R;
			uint8_t G;
			uint8_t B;
		};
		RGB* DirtPtr = new RGB[1024 * 1024 * 3];
		glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_UNSIGNED_BYTE, DirtPtr);
		Error = gluErrorString(glGetError());
		delete[] DirtPtr;
#endif

	glActiveTexture(GL_TEXTURE0);
}

void FTerrainManager::ConstructNonEmptyCellsData()
{
	// Generate feedback vertex buffer
	glGenBuffers(1, &mNonEmptyCellsFeedbackBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, mNonEmptyCellsFeedbackBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(uint32_t) * (FChunk::CHUNK_SIZE + 1) * (FChunk::CHUNK_SIZE + 1) * (FChunk::CHUNK_SIZE + 1), nullptr, GL_DYNAMIC_COPY);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Generate feedback buffer object and bind feedback buffer
	glGenTransformFeedbacks(1, &mNonEmptyCellsFeedbackObject);
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, mNonEmptyCellsFeedbackObject);
		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, mNonEmptyCellsFeedbackBuffer);
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);

	// Generate vertex array object and enable feedback buffer attributes
	glGenVertexArrays(1, &mNonEmptyCellsVAO);
	glBindVertexArray(mNonEmptyCellsVAO);
		glBindBuffer(GL_ARRAY_BUFFER, mNonEmptyCellsFeedbackBuffer);
			glVertexAttribIPointer(GLAttributePosition::x8_y8_z8_case8, 1, GL_UNSIGNED_INT, 0, BUFFER_OFFSET(0));
			glEnableVertexAttribArray(GLAttributePosition::x8_y8_z8_case8);
	glBindVertexArray(0);
}

void FTerrainManager::ConstructListVertsToGenData()
{
	// Generate feedback vertex buffer
	glGenBuffers(1, &mVertsToGenFeedbackBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, mVertsToGenFeedbackBuffer);
		const uint32_t MAX_VERT_OUTPUT = 3;
		glBufferData(GL_ARRAY_BUFFER, MAX_VERT_OUTPUT * sizeof(uint32_t) * (FChunk::CHUNK_SIZE + 1) * (FChunk::CHUNK_SIZE + 1) * (FChunk::CHUNK_SIZE + 1), nullptr, GL_DYNAMIC_COPY);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Generate feedback buffer object and bind feedback buffer
	glGenTransformFeedbacks(1, &mVertsToGenFeedbackObject);
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, mVertsToGenFeedbackObject);
		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, mVertsToGenFeedbackBuffer);
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);

	// Generate vertex array object and enable feedback buffer attributes
	glGenVertexArrays(1, &mVertsToGenVAO);
	glBindVertexArray(mVertsToGenVAO);
		glBindBuffer(GL_ARRAY_BUFFER, mVertsToGenFeedbackBuffer);
			glVertexAttribIPointer(GLAttributePosition::x8_y8_z8_null4_edge4, 1, GL_UNSIGNED_INT, 0, BUFFER_OFFSET(0));
			glEnableVertexAttribArray(GLAttributePosition::x8_y8_z8_null4_edge4);
	glBindVertexArray(0);
}

void FTerrainManager::LoadShaders()
{
	// Load BuildDensitiesPass
	FShader InstancedQuadVertShader{ L"Shaders/InstancedFullScreenQuads.vert.glsl", GL_VERTEX_SHADER };
	FShader DensityFragShader{ L"Shaders/BuildDensitiesPass_WriteTexture.frag.glsl", GL_FRAGMENT_SHADER };
	mBuildDensitiesPass.AttachShader(InstancedQuadVertShader);
	mBuildDensitiesPass.AttachShader(DensityFragShader);
	mBuildDensitiesPass.LinkProgram();

	// Load ListNonEmptyCellsPass
	FShader LNECVertShader{ L"Shaders/ListNonEmptyCellsPass_StreamCells.vert.glsl", GL_VERTEX_SHADER };
	FShader LNECGeoShader{ L"Shaders/ListNonEmptyCellsPass_StreamCells.gs.glsl", GL_GEOMETRY_SHADER };
	mListNonEmptyCellsPass.AttachShader(LNECVertShader);
	mListNonEmptyCellsPass.AttachShader(LNECGeoShader);

	// Set up Transform Feedback for LNECPass
	const uint32_t LNEC_VaryingCount = 1;
	const char* LNEC_Varyings[LNEC_VaryingCount] =
	{
		"x8_y8_z8_case8"
	};
	glTransformFeedbackVaryings(mListNonEmptyCellsPass.GetID(), LNEC_VaryingCount, LNEC_Varyings, GL_INTERLEAVED_ATTRIBS);
	mListNonEmptyCellsPass.LinkProgram();

	// Load ListVertsToGenPass
	FShader LVTGVertShader{ L"Shaders/ListVertsToGenPass_GetVerts.vert.glsl", GL_VERTEX_SHADER };
	FShader LVTGGeomShader{ L"Shaders/ListVertsToGenPass_StreamVerts.gs.glsl", GL_GEOMETRY_SHADER };
	mListVertsToGenPass.AttachShader(LVTGVertShader);
	mListVertsToGenPass.AttachShader(LVTGGeomShader);

	// Set up Transform Feedback for LVTGPass
	const uint32_t LVTG_VaryingCount = 1;
	const char* LVTG_Varyings[LVTG_VaryingCount] =
	{
		"x8_y8_z8_null4_edge4"
	};
	glTransformFeedbackVaryings(mListVertsToGenPass.GetID(), LVTG_VaryingCount, LVTG_Varyings, GL_INTERLEAVED_ATTRIBS);
	mListVertsToGenPass.LinkProgram();

	// Load GenVerticesPass
	FShader GenVertsVertShader{ L"Shaders/GenerateVerticesPass_GenVerts.vert.glsl", GL_VERTEX_SHADER };
	mGenVerticesPass.AttachShader(GenVertsVertShader);

	// Set up Transform Feedback for GenVerticesPass
	const uint32_t GV_VaryingCount = 3;
	const char* GV_Varyings[GV_VaryingCount] =
	{
		"WorldPosition_Ambient",
		"Normal",
		"gl_SkipComponents1" // padding for vec3 alignment
	};
	glTransformFeedbackVaryings(mGenVerticesPass.GetID(), GV_VaryingCount, GV_Varyings, GL_INTERLEAVED_ATTRIBS);
	mGenVerticesPass.LinkProgram();

	// Load SplatVertIdsPass
	FShader SplatVertIDVert{ L"Shaders/SplatVertIdsPass.vert.glsl", GL_VERTEX_SHADER };
	FShader SplatVertIDFrag{ L"Shaders/SplatVertIdsPass_WriteTexture.frag.glsl", GL_FRAGMENT_SHADER };
	mSplatVertIdsPass.AttachShader(SplatVertIDVert);
	mSplatVertIdsPass.AttachShader(SplatVertIDFrag);
	mSplatVertIdsPass.LinkProgram();

	// Load GenIndicesPass
	FShader GenIndicesVertShader{ L"Shaders/GenerateIndices.vert.glsl", GL_VERTEX_SHADER };
	FShader GenIndicesGeomShader{ L"Shaders/GenerateIndices.gs.glsl", GL_GEOMETRY_SHADER };
	mGenIndicesPass.AttachShader(GenIndicesVertShader);
	mGenIndicesPass.AttachShader(GenIndicesGeomShader);

	// Set up Transform Feedback for GenIndicesPass
	const uint32_t GenIndices_VaryingCount = 1;
	const char* GenIndices_Varyings[GenIndices_VaryingCount] =
	{
		"index"
	};
	glTransformFeedbackVaryings(mGenIndicesPass.GetID(), GenIndices_VaryingCount, GenIndices_Varyings, GL_INTERLEAVED_ATTRIBS);
	mGenIndicesPass.LinkProgram();
}

void FTerrainManager::Update()
{
	// Only update visibility list if the camera crossed a chunk
	// boundary.
	ASSERT(FCamera::Main && "No active camera.");
	const Vector3i CameraPosition = FCamera::Main->Transform.GetPosition();
	const Vector3i CameraChunk = CameraPosition / (int32_t)FChunk::CHUNK_SIZE;

	if (mLastCameraChunk != CameraChunk)
	{
		mLastCameraChunk = CameraChunk;
		UpdateVisibleChunks();
	}

	UpdateLoadList();
}

void FTerrainManager::Render()
{
	ConstructRenderList();

	for (const auto& Index : mRenderList)
	{
		mChunks[Index].Render();
	}
}

void FTerrainManager::UpdateVisibleChunks()
{
	// Set the camera position at the min chunk position that is visible.
	// Use this offset in determining the absolute position of each chunk.
	const Vector3i CameraOffset = mLastCameraChunk - Vector3i{ VISIBILITY_DISTANCE, 0, VISIBILITY_DISTANCE };
	mLoadList.clear();

	const int32_t HorizontalBounds = 2 * VISIBILITY_DISTANCE + 1;
	const int32_t VerticalBounds = VISIBILITY_DISTANCE + 1;

	// Load in the horizontal chunk slice that the camera is currently at
	for (int32_t x = 0; x < HorizontalBounds; x++)
	{
		for (int32_t z = 0; z < HorizontalBounds; z++)
		{
			const Vector3i AbsolutePosition = Vector3i{ x, CameraOffset.y, z } + CameraOffset;
			const int32_t Index = GetChunkIndex(AbsolutePosition);

			// Check if the correct chunk is loaded.
			if (mChunkPositions[Index] != AbsolutePosition)
			{
				mLoadList.push_back(AbsolutePosition);
			}
		}
	}

	// Load in the rest of the visible chunks by mirroring across the y axis for each horizontal chunk slice
	for (int32_t y = 1; y < VISIBILITY_DISTANCE / 2; y++)
	{
		// Flip-flop -1 and 1
		for (int32_t Flip = -1; Flip < 2; Flip += 2)
		{
			for (int32_t x = 0; x < HorizontalBounds; x++)
			{
				for (int32_t z = 0; z < HorizontalBounds; z++)
				{
					const Vector3i AbsolutePosition = Vector3i{ x, y * Flip, z } + CameraOffset;
					const int32_t Index = GetChunkIndex(AbsolutePosition);

					// Check if the correct chunk is loaded.
					if (mChunkPositions[Index] != AbsolutePosition)
					{
						mLoadList.push_back(AbsolutePosition);
					}
				}
			}
		}
	}
}

void FTerrainManager::ConstructRenderList()
{
	mRenderList.clear();

	// Get the view volume in chunk coord
	FMatrix4 ToChunkCoord;
	ToChunkCoord.Scale(1.0f / (float)FChunk::CHUNK_SIZE);
	FFrustum ViewVolume = FCamera::Main->GetWorldViewFrustum();
	ViewVolume.TransformBy(ToChunkCoord);

	// Chunk size is now 1
	const float ChunkSizeFloat = (float)1.0f;
	const float HalfChunk = (ChunkSizeFloat / 2.0f);

	const Vector3f HalfChunkVector{ HalfChunk, HalfChunk, HalfChunk };

	for (uint32_t i = 0; i < CHUNK_COUNT; i++)
	{
		const Vector4f Position{ Vector3f{ mChunkPositions[i] } + HalfChunkVector, 1.0f };

		if (!mChunks[i].IsEmpty() && ViewVolume.IsUniformAABBVisible(Position, ChunkSizeFloat))
		{
			mRenderList.push_back(i);
		}
	}
}

void FTerrainManager::UpdateLoadList()
{
	const uint32_t LoadListSize = mLoadList.size();
	if (LoadListSize > 0)
	{
		uint32_t LoadCount = 0;
		glDisable(GL_DEPTH_TEST);
		glViewport(0, 0, DENSITY_TEXTURE_BOUNDS, DENSITY_TEXTURE_BOUNDS);

		for (; LoadCount < MAX_LOADS_PER_FRAME && LoadCount < LoadListSize; LoadCount++)
		{
			const Vector3i ChunkPosition = mLoadList[LoadCount];
			const Vector3i WorldPosition = ChunkPosition * FChunk::CHUNK_SIZE;
			const size_t Index = GetChunkIndex(ChunkPosition);

			// Generate density texture for this chunk position.
			{
				mBuildDensitiesPass.Use();
				mWorldPositionUniform.SetData(0, WorldPosition);
				glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, DENSITY_TEXTURE_BOUNDS);
			}

			// Debug: Check DensityTexture
#ifndef NDEBUG
			glFlush();
			float* DensityValues = new float[DENSITY_TEXTURE_BOUNDS * DENSITY_TEXTURE_BOUNDS * DENSITY_TEXTURE_BOUNDS];
			glBindTexture(GL_TEXTURE_3D, mDensityTexture);
			glGetTexImage(GL_TEXTURE_3D, 0, GL_RED, GL_FLOAT, DensityValues);
			glBindTexture(GL_TEXTURE_3D, 0);
			delete[] DensityValues;
#endif

			glEnable(GL_RASTERIZER_DISCARD);
			// List Non Empty Cells
			GLuint NonEmptyCellCount = ListNonEmptyCells();

			if (NonEmptyCellCount != 0)
			{
				FChunk& Chunk = mChunks[Index];
				Chunk.SetEmpty(std::false_type{});

				// List Verts to Generate
				ListVertsToGen();
				GenerateChunkVertices(Chunk);

				glDisable(GL_RASTERIZER_DISCARD);
				BuildVertexIDTexture();
				glEnable(GL_RASTERIZER_DISCARD);

				GenerateVertexIndices(Chunk);
			}
			else
			{
				mChunks[Index].SetEmpty(std::true_type{});
			}

			glDisable(GL_RASTERIZER_DISCARD);
			mChunkPositions[Index] = ChunkPosition;
		}

		mLoadList.erase(mLoadList.begin(), mLoadList.begin() + LoadCount);
	}
}

GLuint FTerrainManager::ListNonEmptyCells()
{
	mListNonEmptyCellsPass.Use();

	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, mNonEmptyCellsFeedbackObject);

	// Setup query for nonempty cell count
	GLuint NonEmptyCellQuery = 0;
	glGenQueries(1, &NonEmptyCellQuery);
	glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, NonEmptyCellQuery);

	// Begin feedback stage
	glBeginTransformFeedback(GL_POINTS);
	
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_3D, mDensityTexture);

	// Render points at each voxel position
	glBindVertexArray(mChunkPointsVAO);
	glDrawArrays(GL_POINTS, 0, 33 * 33 * 33);

	// End feedback
	glEndTransformFeedback();
	
	// Debug: Checking cell data
#ifndef NDEBUG
	glFlush();
	const uint32_t DATA_SIZE = 33 * 33 * 33;
	struct Cell
	{
		uint32_t Case : 8;
		uint32_t Z : 8;
		uint32_t Y : 8;
		uint32_t X : 8;
	} Cells[33][33][33];
	glGetBufferSubData(GL_TRANSFORM_FEEDBACK_BUFFER, 0, DATA_SIZE * sizeof(Cell), Cells);
	//delete[] Cells;
#endif

	glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);

	// Get and return nonempty cell count
	GLuint NonEmptyCellCount = 0;
	glGetQueryObjectuiv(NonEmptyCellQuery, GL_QUERY_RESULT, &NonEmptyCellCount);
	return NonEmptyCellCount;
}

void FTerrainManager::ListVertsToGen()
{
	mListVertsToGenPass.Use();

	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, mVertsToGenFeedbackObject);

	glBeginTransformFeedback(GL_POINTS);

	glBindVertexArray(mNonEmptyCellsVAO);
	glDrawTransformFeedback(GL_POINTS, mNonEmptyCellsFeedbackObject);

	glEndTransformFeedback();

	// Debug: Checking verts to gen
	// x8_y8_z8_null4_edge4
#ifndef NDEBUG
	glFlush();
	const uint32_t DATA_SIZE = 33 * 33 * 33 * 3;
	struct Cell
	{
		uint32_t Edge : 8;
		uint32_t Z : 8;
		uint32_t Y : 8;
		uint32_t X : 8;
	} Cells[33][33][33 * 3];// = new Cell[DATA_SIZE];
	glGetBufferSubData(GL_TRANSFORM_FEEDBACK_BUFFER, 0, DATA_SIZE * sizeof(Cell), Cells);

	//delete[] Cells;
#endif

	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);
}

void FTerrainManager::GenerateChunkVertices(FChunk& Chunk)
{
	// Generate polygons for the chunk
	mGenVerticesPass.Use();

	// March through verts to gen and send those to the chunk
	Chunk.StartVertexFeedback();
		glBindVertexArray(mVertsToGenVAO);
		glDrawTransformFeedback(GL_POINTS, mVertsToGenFeedbackObject);
	Chunk.EndVertexFeedback();
}

void FTerrainManager::BuildVertexIDTexture()
{
	mSplatVertIdsPass.Use();

	// March through verts to gen and map index data to vertexID texture
	glBindVertexArray(mVertsToGenVAO);
	glDrawTransformFeedback(GL_POINTS, mVertsToGenFeedbackObject);

	// Debug: Check Vertex Ids
#ifndef NDEBUG
	glFlush();
	uint32_t VertexIDs[33][33][33*3];//new uint32_t[3 * 33 * 33 * 33];
	glBindTexture(GL_TEXTURE_3D, mVertexIDVol);
		glGetTexImage(GL_TEXTURE_3D, 0, GL_RED_INTEGER, GL_UNSIGNED_INT, VertexIDs);
	glBindTexture(GL_TEXTURE_3D, 0);
//	delete[] VertexIDs;
#endif
}

void FTerrainManager::GenerateVertexIndices(FChunk& Chunk)
{
	// Generate Indices Pass
	mGenIndicesPass.Use();
	
	Chunk.StartIndexFeedback();
		glBindVertexArray(mNonEmptyCellsVAO);
		glDrawTransformFeedback(GL_POINTS, mNonEmptyCellsFeedbackObject);
	Chunk.EndIndexFeedback();
}