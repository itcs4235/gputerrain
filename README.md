# Procedural Terrain on the GPU #

This is an OpenGL GPU-based procedurally generated terrain application based on the "Generating Complex Procedural Terrains Using the GPU" chapter from [GPU Gems 3](https://developer.nvidia.com/gpugems/GPUGems3/gpugems3_ch01.html). This project is being developed for the ITCS 4235 "Game Engine Construction" course at UNC-Charlotte.

### Project Description ###

* [Project Proposal](https://bytebucket.org/itcs4235/gputerrain/raw/16c2d4b92fc6e885a62145f7497f6110467d32b0/Journal/Project_Proposal_GPUTerrain.pdf?token=47a6af222b144a23eae5405c0da5cced48fe64ea)
* [Final Report](https://bytebucket.org/itcs4235/gputerrain/raw/bf809a144f52b458e5de595d095eb1b6d4d7c43d/Journal/Final_Report_GPUTerrain.pdf)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions